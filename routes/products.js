var express = require('express');
var router = express.Router();
var {
  index,
  newProduct,
  editProduct,
  create,
  update
} = require('./../app/controllers/products-controller');

router.get('/', index);
router.get('/new', newProduct);
router.get('/edit/:id', editProduct);
router.put('/', update);
router.post('/', create);

module.exports = router;
