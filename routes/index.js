var express = require('express');
var router = express.Router();
var {
  index,
  search
} = require('./../app/controllers/index-controller');

router.get('/', index);
router.get('/search', search);

module.exports = router;
