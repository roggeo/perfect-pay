const path = require('path');
const childProcess = require('child-process-promise');
const spawnOptions = { stdio: 'inherit' };

(async () => {
    const config = require(__dirname + '/../config/config.json')[process.env.NODE_ENV];
    const url = 'mysql://root:'+config.password+'@localhost:3306/'+config.database; try {
        await childProcess.spawn('./node_modules/.bin/sequelize', ['db:migrate', `--url=${url}`], spawnOptions);
        console.log('*************************');
        console.log('Migration successful');
    } catch (err) {
        console.log('*************************');
        console.log('Migration failed. Error:', err.message);
        process.exit(1);
    }
    process.exit(0);
})();