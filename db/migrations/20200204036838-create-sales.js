'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Sales', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(11)
      },
      productId: {
        type: Sequelize.INTEGER(11),
        references: {
         model: "Products",
         key: "id"
        }
      },
      customerId: {
        type: Sequelize.INTEGER(11),
        references: {
         model: "Customers",
         key: "id"
        }
      },
      quantity: {
        type: Sequelize.INTEGER(11)
      },
      discount: {
        type: Sequelize.DOUBLE(10,2)
      },
      saleAmount: {
        type: Sequelize.DOUBLE(10,2)
      },
      status: {
        type: Sequelize.INTEGER(1),
        defaultValue: 1
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.fn('now')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Sales');
  }
};