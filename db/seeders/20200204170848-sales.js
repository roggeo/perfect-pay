'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Sales', [{
      id: 1,    
      productId: 1,
      customerId: 1,
      quantity: 5,
      discount: 1.5,
      saleAmount: 7502.50
    },
    {
      id: 2,    
      productId: 2,
      customerId: 1,
      quantity: 3,
      discount: 1.5,
      saleAmount: 7802.40,
      status: 2
    },
    {
      id: 3,    
      productId: 1,
      customerId: 2,
      quantity: 2,
      discount: 1.5,
      saleAmount: 3001.00
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    
  }
};