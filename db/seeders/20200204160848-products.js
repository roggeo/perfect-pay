'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Products', [{
      id: 1,
      name: 'Geladeira',
      description: 'Geladeira branca 200 Lt',
      price: 1500.50
    },
    {
      id: 2,
      name: 'Notebook Dell 15',
      description: 'Notebook intel i-7 placa de vídeo',
      price: 2600.80
    },
    {
      id: 3,
      name: 'SmartTV Sony',
      description: 'SmartTY Sony 72',
      price: 4300.60
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    
  }
};