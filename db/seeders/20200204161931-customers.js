'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('Customers', [{
      id: 1,
      name: 'Bryan Enrico Marcos Peixoto',
      identificationType: 'RG',
      identificationNumber: '33.760.045-4',
      email: 'bryanenricomarcospeixoto_@isbt.com.br'
    },
    {
      id: 2,
      name: 'Sebastião Daniel Nascimento',
      identificationType: 'CPF',
      identificationNumber: '422.300.860-90',
      email: 'sebastiaodanielnascimento-90@rjnet.com.br'
    },
    {
      id: 3,
      name: 'Samuel Murilo Elias Santos',
      identificationType: 'RG',
      identificationNumber: '37.924.118-3',
      email: 'samuelmuriloeliassantos__samuelmuriloeliassantos@tecnew.net'
    }], {});

  },

  down: (queryInterface, Sequelize) => {

  }
};
