process.env.NODE_ENV = 'test';

var assert = require('assert');
var models = require('./../app/models');
var errorParser = require('./../app/utils/error-parser-sequelize');

const CustomerOne = {
    id: 1,
    name: 'Bryan Enrico Marcos Peixoto',
    identificationType: 'RG',
    identificationNumber: '33.760.045-4',
    email: 'bryanenricomarcospeixoto_@isbt.com.br'
};

const CustomerTwo = {
    id: 2,
    name: 'Pedro Henrique Vinicius Peixoto',
    identificationType: 'RG',
    identificationNumber: '09999999999999990999999999099999999999909',
    email: 'pedrohenrique.com.br'
};

let sales = models.Sales.findAll()
    .then(function(datas) {                
        datas.forEach(element => {
            let idData = element.id;
            models.Sales.destroy(
                { where: {id: idData} }
            ).catch((err) => {
                console.log(err);
            })
        });
    
    });
    
let customers = models.Customers.findAll()
    .then(function(datas) {                
        datas.forEach(element => {
            let idData = element.id;
            models.Customers.destroy(
                { where: {id: idData} }
            ).catch((err) => {
                console.log(err);
            })
        });
    
    });

const clearTable = Promise.all([sales, customers]);

describe('Customers', () => {

    describe('# Criar Customers', () => {

        it('Criar novo Customer', () => {   
            clearTable.then(() => {         
                models.Customers.create(CustomerOne).then((datas) => {
                    assert.ok(true);
                }).catch((err) => {
                    console.log( err );
                })
            })
        });

        it('Validar dados não permitidos de Customers', () => {
            models.Customers.create(CustomerTwo).then((datas) => {
                assert.ok(false);
            }).catch((err) => {
                const errors = errorParser(err);                
                if ('errors' in errors) {
                    assert.ok(true);
                }
            })
        });
    });

    describe('# Exibir Customers', () => {

        it('Listar todos Customers', () => {
            models.Customers.findAll().then((datas) => {
                //console.log(datas);
                assert.ok(true);
            }).catch((err) => {
                assert.ok(false);
                console.log(err);
            })
        });

        it('Listar um Customer', () => {
            models.Customers.findOne({where: {id: 1}}).then((datas) => {
                //console.log(datas);
                assert.ok(true);
            }).catch((err) => {
                assert.ok(false);
                console.log(err);
            })
        });
    });

});