process.env.NODE_ENV = 'test';

var assert = require('assert');
var models = require('./../app/models');
var errorParser = require('./../app/utils/error-parser-sequelize');
var Products = require('./../app/models/products');
var Sales = require('./../app/models/sales');

let SaleOne = {
    id: 1,    
    productId: 1,
    customerId: 1,
    quantity: 5,
    discount: 1.5,
    saleAmount: 12.25
};

let SaleTwo = {
    id: 1,    
    productId: 1,
    customerId: 1,
    quantity: 'uma string',
    discount: 'uma string',
    saleAmount: 'uma string'
};

const clearTable = models.Sales.findAll()
    .then(function(datas) {                
        datas.forEach(element => {
            let idData = element.id;
            models.Sales.destroy(
                { where: {id: idData} }
            ).catch((err) => {
                console.log(err);
            })
        });
    
    }).catch((err) => {
        console.log(err);
    });



describe('Sales', () => {

    describe('# Criar Sales', () => {

        it('Criar novo Sales', () => {
            clearTable.then(() => {
                models.Sales.create(SaleOne).then((datas) => {
                    assert.ok(true);
                }).catch((err) => {
                    console.log( err );
                })
            })            
        });

        it('Validar dados não permitidos de Sales', () => {
            models.Sales.create(SaleTwo).then((datas) => {
                assert.ok(false);
            }).catch((err) => {
                const errors = errorParser(err);                
                if ('errors' in errors) {
                    assert.ok(true);
                }
            })
        });
    });

    describe('# Exibir Sales', () => {

        it('Listar todos Sales', () => {
            models.Sales.findAll().then((datas) => {
                //console.log(datas);
                assert.ok(true);
            }).catch((err) => {
                assert.ok(false);
                console.log(err);
            })
        });

        it('Listar um Sale', () => {
            models.Sales.findOne({where: {id: 1}}).then((datas) => {
                //console.log(datas);
                assert.ok(true);
            }).catch((err) => {
                assert.ok(false);
                console.log(err);
            })
        });
    });

});