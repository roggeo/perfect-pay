process.env.NODE_ENV = 'test';

var assert = require('assert');
var models = require('./../app/models');
var errorParser = require('./../app/utils/error-parser-sequelize');

const ProductOne = {
    id: 1,
    name: 'Produto 1',
    description: 'Descrição do produto 1',
    price: 20.25
};

const ProductTwo = {
    id: 2,
    name: 'P',
    description: 'Descrição do produto 1',
    price: 'umastring'
};

let sales = models.Sales.findAll()
    .then(function(datas) {                
        datas.forEach(element => {
            let idData = element.id;
            models.Sales.destroy(
                { where: {id: idData} }
            ).catch((err) => {
                console.log(err);
            })
        });
    
    });
    
let products = models.Products.findAll()
    .then(function(datas) {                
        datas.forEach(element => {
            let idData = element.id;
            models.Products.destroy(
                { where: {id: idData} }
            ).catch((err) => {
                console.log(err);
            })
        });
    
    });


const clearTable = Promise.all([sales, products]);

describe('Products', () => {

    describe('# Criar Products', () => {

        it('Criar novo Product', () => {
            clearTable.then(() => {         
                models.Products.create(ProductOne).then((datas) => {
                    assert.ok(true);
                }).catch((err) => {
                    console.log( err );
                })
            })
        });

        it('Validar dados não permitidos de Products', () => {
            models.Products.create(ProductTwo).then((datas) => {
                assert.ok(false);
            }).catch((err) => {
                const errors = errorParser(err);                
                if ('errors' in errors) {
                    assert.ok(true);
                }
            })
        });
    });

    describe('# Exibir Products', () => {

        it('Listar todos Products', () => {
            models.Products.findAll().then((datas) => {
                //console.log(datas);
                assert.ok(true);
            }).catch((err) => {
                assert.ok(false);
                console.log(err);
            })
        });

        it('Listar um Product', () => {
            models.Products.findOne({where: {id: 1}}).then((datas) => {
                //console.log(datas);
                assert.ok(true);
            }).catch((err) => {
                assert.ok(false);
                console.log(err);
            })
        });
    });

});