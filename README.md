# Perfect Pay - Resumo de Vendas

## Requisitos

- MySql
- Node.js
- Yarn

# Perfect Pay - Resumo de Vendas

## Requisitos

- MySql
- Node.js
- Yarn

## Instalação

### 1. Criar Banco dados:

    $ mysql -u root -p
    $ CREATE DATABASE perfect_pay CHARACTER SET utf8 COLLATE utf8_general_ci;

### 2. Configuração de acesso ao banco de dados

Insira os dados de acesso na chave `development` do arquivo `config/config.json`.

### 3. Migração e criação de dados

É importante executar os comandos a baixo. Pois isso vai criar as tabelas no banco de dados e inserir dados de simulação. Caso não queira criar dados de simulação, execute apenas o primeiro comando.

    $ npx sequelize-cli db:migrate
    $ npx sequelize-cli db:seed:all

### 4. Subir servidor local:

    $ yarn
    $ yarn start

    # Ou para desenvolvimento
    $ yarn
    $ yarn run nodemon

### 5. Acessar a aplicação

A aplicação está disponível para acesso na url `http://localhost:3000`.


## Teste

### 1. Criar Banco dados:

    $ mysql -u root -p
    $ CREATE DATABASE perfect_pay_test CHARACTER SET utf8 COLLATE utf8_general_ci;

### 2. Configuração de acesso ao banco de dados

Insira os dados de acesso na chave `test` do arquivo `config/config.json`.

### 3. Executar teste

    $ yarn test


