const errorsForView = require('./../utils/error-parser-sequelize');
const searchProvider = require('./../services/general-search-provider');
const customersProvider = require('./../services/customers-provider');

let getParams = params => {    
    return {
        product: params.product || null,
        date: params.date || null,
        price: params.price || null
    }
}

// GET /
exports.index =  async (req, res, next) => {
    let params = getParams(req.query);
    let {sales,  products} = await searchProvider(params);
    let customers = await customersProvider.getCustomers();
    
    res.render('index/index', {
        title: 'Vendas',
        sales: sales,
        products: products,
        customers: customers,
        query: {
            customerId: req.query.customer || '',
            date:  req.query.date || ''
        }
    })
}

// GET /search
exports.search =  (req, res, next) => {
    res.render('index/search', {
        title: 'Pesquisar vendas',
        query: {
            customerId: req.query.customer || '',
            date:  req.query.date || ''
        }
    })
}