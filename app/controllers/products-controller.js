const models = require('./../models');
const errorsForView = require('./../utils/error-parser-sequelize');

let paramsProducts = (data) => {
    return {
        companyName:       data.companyName,
        companyNickname:   data.companyNickname,
        companyBirth:      data.companyBirth,
        companyEmail:      data.companyEmail
    }
}

// GET /
exports.index =  (req, res, next) => {
    res.render('products/index', {title: 'Listagem de produtos'})
}

// GET /new
exports.newProduct =  (req, res, next) => {
    res.render('products/new', {title: 'Novo produto'})
}

// GET /edit
exports.editProduct =  (req, res, next) => {
    let id = req.params.id
    res.render('products/edit', {
        productId: id,
        title: 'Editar produto'
    })
}

// POST /products
exports.create =  (req, res, next) => {
    models.Products.create(paramsProducts(req.body)).then((datas) => {
        res.json( datas );
    }).catch((err) => {
        res.status(400);
        res.json(errorsForView(err));
    })
}

// PUT /products/1
exports.update = (req, res, next) => {
    let idData = req.params.id
    models.Products.update(
        paramsProducts(req.body),
        {where: {id: idData}}
    ).then((numRows) => {
        if (numRows[0] > 0) {
            models.Products.findOne({where: {id: idData}}).then((datas) => {
                res.json( datas );
            });
        } else {
            res.status(400);
            res.json( {errors: {update: 'No any fields updated'}});
        }
    }).catch((err) => {
        res.status(400);
        res.json(errorsForView(err));
    })
}