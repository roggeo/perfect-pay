const moment = require('moment');
const formatMoney = require('./../utils//format-money');

const {
    getSaleType
} = require('./../services/data-provider');

exports.dateFormat = value => {
    return moment(value).format('DD/MM/YYYY ');
}

exports.moneyFormat = (value) => {
    return formatMoney.currencyToBrazil(value);
}

exports.saleType = value => {
    return getSaleType(value);
}