const errorValidation = (err) => {
    return {errors: err.errors.map((val) => {               
        let errors = {
            [val.path]:  val.message,
            [val.validatorName]: val.validatorArgs,
            value: val.value,
        };
        return errors
    })};
}

module.exports = (err) => {
    if ('name' in err && err.name === 'SequelizeValidationError') {
        return errorValidation(err);
    }
    
    return (err);
}