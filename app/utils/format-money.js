exports.currencyToBrazil = (value, nameCurrency = 'R$') => {
    var currency = value.toFixed(2).split('.');
    currency[0] = nameCurrency + ' ' + currency[0].split(/(?=(?:...)*$)/).join('.');
    return currency.join(',');
}

exports.currencyToFloat = (value) => {
    var currency = value.replace('.','');
    currency = currency.replace(',','.');
    return currency;
}