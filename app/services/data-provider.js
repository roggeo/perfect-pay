exports.getSaleType = (value) => {
    let type = '';
    switch (value) {
        case 1: type = 'Vendidos'; break;
        case 2: type = 'Cancelados'; break;
        case 3: type = 'Devoluções'; break;
        default: break;
    }
    return type;
}