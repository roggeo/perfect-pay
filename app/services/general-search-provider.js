const models = require('./../models');
const formatMoney = require('./../utils//format-money');

const search = async (params) => {

    let _sales = [];
    let _products = [];
    let _paramsSales = {};
    let _paramsProduct = {};

    if (params.product) {
        _paramsProduct.name = {
            [models.Sequelize.Op.like]: ['%'+ params.product +'%']
        }
    }

    if (params.price) {
        _paramsProduct.price = formatMoney.currencyToFloat(params.price);
    }

    if (params.date) {
        _paramsSales.createdAt = params.date
    }

    if (params.customer) {
        _paramsSales.customerId = params.customer
    }

    let findProducts = await models.Products.findAll({
        where: _paramsProduct,
        include: [{
           model: models.Sales,
           where: _paramsSales
        }]
    }).then((datas) => {
        _products = datas;
        _sales = datas.Sales
    }).catch((err) => {
        console.log(err);
    });

    let findSales = await models.Sales.findAll({
        where: _paramsSales,
        include: [{
            model: models.Products,
            where: _paramsProduct
        }]
    }).then((datas) => {
        _sales = datas;
    }).catch((err) => {
        console.log(err);
    });

    return {
        sales: _sales,
        products: _products
    }
}

module.exports = search;