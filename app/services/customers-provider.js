const models = require('./../models');

exports.getCustomers = async () => {
    let _customers = [];    
    let findCustomers = await models.Customers.findAll().then((datas) => {
        _customers = datas;
    }).catch((err) => {
        console.log(err);
    });
    return _customers;
}