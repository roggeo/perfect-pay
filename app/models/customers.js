'use strict';
module.exports = (sequelize, DataTypes) => {
  const Customers = sequelize.define('Customers', {
    name: {
      type: DataTypes.STRING,
      validate: {
        len: [3,200]
      }
    },
    identificationType: {
      type: DataTypes.STRING,
      validate: {
        isAlpha: true,
        len: [2,100]
      }
    },
    identificationNumber: {
      type: DataTypes.STRING,
      validate: {
        len: [2,100]
      }
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true,
        len: [5,200]
      }
    }
  }, {});
  Customers.associate = function(models) {
    Customers.hasMany(models.Sales);
  };
  return Customers;
};