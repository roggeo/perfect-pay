'use strict';
module.exports = (sequelize, DataTypes) => {
  const Products = sequelize.define('Products', {
    name: {
      type: DataTypes.STRING,
      validate: {
        len: [3,200]
      }
    },
    description: DataTypes.TEXT,
    price: {
      type: DataTypes.DOUBLE,
      isDecimal: true
    }
  }, {});
  Products.associate = function(models) {
    Products.hasMany(models.Sales);
  };
  return Products;
};