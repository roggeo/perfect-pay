'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sales = sequelize.define('Sales', {
    productId: DataTypes.INTEGER,
    customerId: DataTypes.INTEGER,
    quantity: {
      type: DataTypes.INTEGER,
      isNumeric: true,
      len: [1,11]
    },
    discount: {
      type: DataTypes.DOUBLE,
      isDecimal: true
    },
    saleAmount: {
      type: DataTypes.DOUBLE,
      isDecimal: true
    },
    status: DataTypes.INTEGER
  }, {});
  Sales.associate = function(models) {
    Sales.belongsTo(models.Products);
    Sales.belongsTo(models.Customers);
  };
  return Sales;
};